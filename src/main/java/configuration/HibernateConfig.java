package configuration;

import java.util.Properties;

import javax.sql.DataSource;
 
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import static utils.Constants.HIBERNATE_DIALECT_PROPERTY;
import static utils.Constants.HIBERNATE_FORMAT_PROPERTY;
import static utils.Constants.HIBERNATE_SHOW_PROPERTY;
import static utils.Constants.JDBC_DRIVER_CLASS_NAME_PROPERTY;
import static utils.Constants.JDBC_PASSWORD_PROPERTY;
import static utils.Constants.JDBC_URL_PROPERTY;
import static utils.Constants.JDBC_USERNAME_PROPERTY;

@Configuration
@EnableTransactionManagement
@ComponentScan({ "configuration" })
@PropertySource(value = { "classpath:application.properties" })
public class HibernateConfig {

    @Autowired
    private Environment environment;
    
    @Bean
    public LocalSessionFactoryBean sessionFactory() {
      LocalSessionFactoryBean sfb = new LocalSessionFactoryBean();
      sfb.setDataSource(dataSource());
      sfb.setPackagesToScan(new String[] { "model" });
      Properties props = new Properties();
      props.put(HIBERNATE_DIALECT_PROPERTY, environment.getRequiredProperty(HIBERNATE_DIALECT_PROPERTY));
      props.put(HIBERNATE_SHOW_PROPERTY, environment.getRequiredProperty(HIBERNATE_SHOW_PROPERTY));
      props.put(HIBERNATE_FORMAT_PROPERTY, environment.getRequiredProperty(HIBERNATE_FORMAT_PROPERTY));
      sfb.setHibernateProperties(props);
      return sfb;
    }
     
    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(environment.getRequiredProperty(JDBC_DRIVER_CLASS_NAME_PROPERTY));
        dataSource.setUrl(environment.getRequiredProperty(JDBC_URL_PROPERTY));
        dataSource.setUsername(environment.getRequiredProperty(JDBC_USERNAME_PROPERTY));
        dataSource.setPassword(environment.getRequiredProperty(JDBC_PASSWORD_PROPERTY));
        return dataSource;
    }
    
    @Bean
    public HibernateTransactionManager txManager(SessionFactory sf) {
        return new HibernateTransactionManager(sf);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
