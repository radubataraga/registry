package configuration;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.config.annotation.*;

import static utils.Constants.MESSAGES_FILE_NAME;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"controller","configuration","dao","model", "service.impl"})
public class AppConfig extends WebMvcConfigurerAdapter{

	private static final String VIEW_RESOLVER_PREFIX = "/WEB-INF/views/";
	private static final String VIEW_RESOLVER_SUFIX = ".jsp";
	private static final String RESOURCES_HANDLER = "/resources/**";
	private static final String RESOURCES_LOCATION = "/resources/";
	private static final String WEBJARS_HANDLER = "/webjars/**";
	private static final String WEBJAR_RESOURCE_LOCATION = "classpath:/META-INF/resources/webjars/";

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
	    registry.addResourceHandler(RESOURCES_HANDLER).addResourceLocations(RESOURCES_LOCATION);
	    registry.addResourceHandler(WEBJARS_HANDLER).addResourceLocations(WEBJAR_RESOURCE_LOCATION);
	}

	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
	    configurer.enable();
	}
	
    @Bean
    public ViewResolver viewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setViewClass(JstlView.class);
        viewResolver.setPrefix(VIEW_RESOLVER_PREFIX);
        viewResolver.setSuffix(VIEW_RESOLVER_SUFIX);
        return viewResolver;
    }
     
    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename(MESSAGES_FILE_NAME);
        return messageSource;
    }

    @Bean
	public MessageSourceAccessor accessor(){
		return new MessageSourceAccessor(messageSource());
	}
}
