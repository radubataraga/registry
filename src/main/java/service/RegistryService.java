
package service;

import exception.UserAlreadyExistException;
import model.Appuser;

public interface RegistryService {

	public void register(Appuser appuser) throws UserAlreadyExistException;
}
