
package service.impl;

import dao.UserDao;
import exception.UserAlreadyExistException;
import model.Appuser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import service.RegistryService;

@Service("registryService")
public class RegistryServiceImpl implements RegistryService {

	@Autowired
	private UserDao userDao;

	@Autowired
	private PasswordEncoder passwordEncoder;

	/**
	 * Register the user if is not registered already by email or username.
	 * @param appuser The Appuser that gets registered
	 * @throws UserAlreadyExistException 
	 */
	public void register(Appuser appuser) throws UserAlreadyExistException {

		if (!isUserAlreadyRegistered(appuser.getUsername(), appuser.getEmail())) {
			appuser.setPassword(passwordEncoder.encode(appuser.getPassword()));
			userDao.saveIn(appuser);
		} else {
			throw new UserAlreadyExistException();
		}
	}

	private boolean isUserAlreadyRegistered(String username, String email) {

		boolean isUserRegistered = false;
		Appuser foundUserByUsername = userDao.findUserByUsername(username);
		Appuser foundUserByMail = userDao.findUserByEmail(email);
		if (foundUserByMail != null || foundUserByUsername != null) {
			isUserRegistered = true;
		}
		return isUserRegistered;
	}
}
