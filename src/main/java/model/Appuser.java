package model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.Date;

import org.hibernate.validator.constraints.Email;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="APPUSER")
public class Appuser {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@NotNull
	@Size(min=3, max=50)
	@Column(name = "FIRSTNAME", nullable = false)
	private String firstname;

	@NotNull
	@Size(min=3, max=50)
	@Column(name = "SURNAME", nullable = false)
	private String surname;

	@NotNull
	@DateTimeFormat(pattern="dd.MM.yyyy")
	@Column(name = "BIRTHDATE", nullable = false)
	private Date birthdate;

	@NotNull
	@Size(min=3, max=50)
	@Column(name = "GENDER", nullable = false)
	private String gender;

	@NotNull
	@Size(min=3, max=50)
	@Column(name = "EMAIL", nullable = false, unique = true)
	@Email
	private String email;

	@Size(min=3, max=50)
	@Column(name = "USERNAME", nullable = false)
	@NotNull
	private String username;

	@Size(min=3, max=60)
	@Column(name = "PASSWORD", nullable = false)
	@NotNull
	private String password;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
