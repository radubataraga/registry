
package utils;

public class Constants {

	//	endpoints
	public static final String REGISTRATION_ENDPOINT = "/registration";
	public static final String SUCCESS_REGISTRATION_ENDPOINT = "/successRegistration";
	public static final String SUCCESS_REGISTRATION_SERVLET_ENDPOINT = "/successRegistration\"";

	//	Model data name
	public static final String APPUSER_MODEL_NAME = "appuser";
	public static final String MODEL_SUCCESS_STRING = "success";

	//	message property names
	public static final String SUCCESSFUL_REGISTRATION_MESSAGE_PROPERTY = "successful.registration";

	//	hibernate properties name
	public static final String HIBERNATE_DIALECT_PROPERTY = "hibernate.dialect";
	public static final String HIBERNATE_SHOW_PROPERTY = "hibernate.show_sql";
	public static final String HIBERNATE_FORMAT_PROPERTY = "hibernate.format_sql";

	//	jdbc properties names
	public static final String JDBC_DRIVER_CLASS_NAME_PROPERTY = "jdbc.driverClassName";
	public static final String JDBC_URL_PROPERTY = "jdbc.url";
	public static final String JDBC_USERNAME_PROPERTY = "jdbc.username";
	public static final String JDBC_PASSWORD_PROPERTY = "jdbc.password";

	// Messages file name
	public static final String MESSAGES_FILE_NAME = "messages";
}

