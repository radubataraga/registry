package controller;

import model.Appuser;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import utils.Constants;

import static utils.Constants.APPUSER_MODEL_NAME;

@ControllerAdvice
public class ExceptionHandlerController {

    @ExceptionHandler(value = Exception.class)
    public ModelAndView inputValidationError() {
        return new ModelAndView(Constants.REGISTRATION_ENDPOINT, APPUSER_MODEL_NAME, new Appuser());
    }

}
