package controller;

import javax.validation.Valid;

import exception.UserAlreadyExistException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import model.Appuser;
import service.RegistryService;
import utils.Constants;

import static utils.Constants.APPUSER_MODEL_NAME;
import static utils.Constants.MODEL_SUCCESS_STRING;
import static utils.Constants.SUCCESSFUL_REGISTRATION_MESSAGE_PROPERTY;
import static utils.Constants.SUCCESS_REGISTRATION_ENDPOINT;

@Controller
@RequestMapping("/")
public class AppController {

	@Autowired
	private RegistryService registryService;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private MessageSourceAccessor accessor;

	/*
	 * This method will provide a registration form for new user.
	 */
	@RequestMapping(value = { "/", Constants.REGISTRATION_ENDPOINT }, method = RequestMethod.GET)
	public ModelAndView newUser() {
		return new ModelAndView(Constants.REGISTRATION_ENDPOINT, APPUSER_MODEL_NAME, new Appuser());
	}

	/*
	 * This method will be called on form submission, handling POST request for
	 * saving user in database. It also validates the user input.
	 */
	@RequestMapping(value = { "/", Constants.REGISTRATION_ENDPOINT }, method = RequestMethod.POST)
	public ModelAndView saveUser(@Valid Appuser appuser, BindingResult result) throws UserAlreadyExistException {
		ModelAndView model = new ModelAndView();
		if (result.hasErrors()) {
			model.addObject(APPUSER_MODEL_NAME, appuser);
			model.setViewName(Constants.REGISTRATION_ENDPOINT);
		} else {
			registryService.register(appuser);
			model.setViewName(Constants.SUCCESS_REGISTRATION_ENDPOINT);
		}
		return model;
	}

	/*
	 * This method will redirect the user to a welcome homepage after a successful registration.
	 */
	@RequestMapping(value = SUCCESS_REGISTRATION_ENDPOINT, method = RequestMethod.GET)
	public ModelAndView registered() {
		String messageSuccessful = accessor.getMessage(SUCCESSFUL_REGISTRATION_MESSAGE_PROPERTY);
		return new ModelAndView(Constants.SUCCESS_REGISTRATION_SERVLET_ENDPOINT, MODEL_SUCCESS_STRING, messageSuccessful);
	}

}