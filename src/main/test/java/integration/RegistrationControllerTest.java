
package integration;

import com.google.gson.Gson;
import configuration.AppConfig;
import configuration.AppInitializer;
import configuration.HibernateConfig;
import model.Appuser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;
import java.util.Random;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static utils.Constants.APPUSER_MODEL_NAME;
import static utils.Constants.MODEL_SUCCESS_STRING;
import static utils.Constants.REGISTRATION_ENDPOINT;
import static utils.Constants.SUCCESS_REGISTRATION_ENDPOINT;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes ={ AppConfig.class , HibernateConfig.class, AppInitializer.class })
public class RegistrationControllerTest {

	protected MediaType contentType;
	protected MockMvc mockMvc;

	@Autowired private WebApplicationContext webApplicationContext;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		contentType = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype());
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void testSuccesfulRegistrationRedirectTest() throws Exception {
		ResultActions succesfulRegistration = this.mockMvc.perform(get(SUCCESS_REGISTRATION_ENDPOINT));

		succesfulRegistration.andExpect(status().isOk());

		succesfulRegistration.andExpect(MockMvcResultMatchers.model().attributeExists(MODEL_SUCCESS_STRING));
		succesfulRegistration.andExpect(MockMvcResultMatchers.model().attribute(MODEL_SUCCESS_STRING, "Successful Registration"));
		succesfulRegistration.andExpect(view().name("/successRegistration\""));
	}

	@Test
	public void testSuccesfulRegistration() throws Exception {
		Appuser appuser = new Appuser();
		appuser.setEmail("asda@asdas");
		appuser.setPassword("asdsd");
		appuser.setBirthdate(new Date());
		appuser.setFirstname("asda");
		appuser.setGender("as");
		appuser.setId(100);
		Gson gson = new Gson();
		String json = gson.toJson(appuser);
		ResultActions registration = this.mockMvc.perform(post(REGISTRATION_ENDPOINT).contentType(contentType).content(json));
		registration.andExpect(status().isOk());
		registration.andExpect(view().name("/registration"));
		registration.andExpect(MockMvcResultMatchers.model().attributeExists(APPUSER_MODEL_NAME));
	}

	@Test
	public void testAlreadyRegisteredUser() throws Exception {
		Appuser appuser = new Appuser();
		Random random = new Random();
		int id = Math.abs(random.nextInt());
		appuser.setEmail("asda@asdas");
		appuser.setPassword("asdsd");
		appuser.setBirthdate(new Date());
		appuser.setFirstname("asda");
		appuser.setGender("asa");
		appuser.setSurname("surname");
		appuser.setUsername("username");
		appuser.setId(id);
		Gson gson = new Gson();
		String json = gson.toJson(appuser);
		ResultActions registration = this.mockMvc.perform(post(REGISTRATION_ENDPOINT).contentType(contentType).content(json));
		registration.andExpect(status().isOk());
		registration.andExpect(view().name("/registration"));
		registration.andExpect(MockMvcResultMatchers.model().errorCount(0));
	}
}
